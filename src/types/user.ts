export interface Player {
  name: string;
  width: number;
  height: number;
  move: string;
  orientation: string;
  collectedTreasures: number;
}

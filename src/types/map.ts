export interface Treasure {
  width: number;
  height: number;
  numberOfTreasure: number;
}

export interface Mountain {
  width: number;
  height: number;
}

export interface Map {
  width: number;
  height: number;
  loopNumber: number;
  numberOfTabulations: number;

  treasures: Treasure[];
  mountains: Mountain[];
}

export interface Box {
  type: string; // Plaines (.), Treasure (T), Player A(username)
  treasures: number;
  player: string;
}

import { Map, Mountain, Treasure } from "../types/map";
import { Player } from "../types/user";

const getNumberOfTabulation = (name: string): number => {
  return Math.ceil(name.length / 4);
};

export const getData = (lines: string[]): { players: Player[]; map: Map } => {
  const map: Map = {
    width: 0,
    height: 0,
    loopNumber: 0,
    treasures: [],
    mountains: [],
    numberOfTabulations: 0,
  };
  const players: Player[] = [];

  lines.forEach((line: string) => {
    const formatedLine: string[] = line.replace(/[\n\t\r ]/g, "").split("-");

    if (formatedLine[0][0] === "C") {
      map.width = parseInt(formatedLine[1]);
      map.height = parseInt(formatedLine[2]);
    } else if (formatedLine[0][0] === "M") {
      const mountain: Mountain = {
        width: parseInt(formatedLine[1]),
        height: parseInt(formatedLine[2]),
      };

      map.mountains.push(mountain);
    } else if (formatedLine[0][0] === "T") {
      const treasure: Treasure = {
        width: parseInt(formatedLine[1]),
        height: parseInt(formatedLine[2]),
        numberOfTreasure: parseInt(formatedLine[3]),
      };

      map.treasures.push(treasure);
    } else if (formatedLine[0][0] === "A") {
      const numberOfTabulations = getNumberOfTabulation(formatedLine[1]);

      if (map.loopNumber < formatedLine[5].length) {
        map.loopNumber = formatedLine[5].length;
      }
      if (map.numberOfTabulations < numberOfTabulations) {
        map.numberOfTabulations = numberOfTabulations;
      }

      const player: Player = {
        name: formatedLine[1],
        width: parseInt(formatedLine[2]),
        height: parseInt(formatedLine[3]),
        orientation: formatedLine[4][0],
        move: formatedLine[5],
        collectedTreasures: 0,
      };

      players.push(player);
    }
  });

  return { players, map };
};

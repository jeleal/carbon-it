import { exitWithErrorMessage } from "./errorMessage";
import fs from "fs";
import { ERROR_DOUBLE_MAP_LINE, ERROR_MAP_LINE } from "../constant/mapError";
import { ERROR_PLAYER_LINE } from "../constant/playerError";

export const isValidNumber = (number: string): boolean => {
  return !isNaN(parseInt(number));
};

export const isValidMap = (line: string[], index: number): void => {
  // Check if the first character is a valid argument
  if (line[0][0] !== "C" && line[0][0] !== "M") {
    exitWithErrorMessage(
      `Invalid parameters for the line ${index + 1}: ${line.join(" - ")}`
    );
  }
  // Checks if arguments 1 and 2 are numbers
  if (!isValidNumber(line[1]) || !isValidNumber(line[2])) {
    exitWithErrorMessage(
      `Invalid map for the line ${index + 1}: ${line.join(" - ")}`
    );
  }

  if (
    (parseInt(line[1]) === 0 || parseInt(line[2]) === 0) &&
    line[0][0] === "C"
  ) {
    exitWithErrorMessage(
      `Invalid map for the line ${index + 1}: ${line.join(
        " - "
      )}\nThe width and height must be greater than 0`
    );
  }
};

export const isValidTreasure = (line: string[], index: number): void => {
  if (line[0][0] !== "T") {
    exitWithErrorMessage(
      `Invalid treasure for the line ${index + 1}: ${line.join(" - ")}`
    );
  }
  if (
    !isValidNumber(line[1]) ||
    !isValidNumber(line[2]) ||
    !isValidNumber(line[3])
  ) {
    exitWithErrorMessage(
      `Invalid treasure for the line ${index + 1}: ${line.join(" - ")}`
    );
  }
};

export const isValidAdventurer = (line: string[], index: number): void => {
  if (line[0][0] !== "A") {
    exitWithErrorMessage(
      `Invalid adventurer for the line ${index + 1}: ${line.join(" - ")}`
    );
  } else if (!isValidNumber(line[2]) || !isValidNumber(line[3])) {
    exitWithErrorMessage(
      `Invalid adventurer for the line ${index + 1}: ${line.join(" - ")}`
    );
  } else if (!["N", "S", "O", "E"].includes(line[4][0])) {
    exitWithErrorMessage(
      `Invalid adventurer for the line ${index + 1}: ${line.join(" - ")}`
    );
  } else if (line[5].match(/^[AGD]+$/g) === null) {
    exitWithErrorMessage(
      `Invalid move for the line ${index + 1}: ${line.join(" - ")}`
    );
  }
};

export const isValidFilename = (): void => {
  if (process.argv.length < 3) {
    exitWithErrorMessage("You must enter the file name");
  }

  const filename = process.argv[2];
  fs.stat(filename, (err, _) => {
    if (err) {
      exitWithErrorMessage(err.message);
    }
  });
};

export const isValidData = (lines: string[]) => {
  let mapDetailsExist: number = 0;
  let playerDetailsExist: boolean = false;

  lines.forEach((line: string, index: number) => {
    // Remove all white space from the string and split
    const formatedLine: string[] = line.replace(/[\n\t\r ]/g, "").split("-");

    if (formatedLine[0][0] === "C") {
      mapDetailsExist += 1;
    }
    if (!playerDetailsExist && formatedLine[0][0] === "A") {
      playerDetailsExist = true;
    }
    if (formatedLine.length === 3) {
      isValidMap(formatedLine, index);
    } else if (formatedLine.length === 4) {
      isValidTreasure(formatedLine, index);
    } else if (formatedLine.length === 6) {
      isValidAdventurer(formatedLine, index);
    } else if (formatedLine[0].length !== 0) {
      // !== 0 if is different of an empty string
      exitWithErrorMessage(
        `Invalid parameters for the line ${index + 1}: ${line}`
      );
    }
  });

  if (!mapDetailsExist) {
    exitWithErrorMessage(ERROR_MAP_LINE);
  } else if (mapDetailsExist > 1) {
    exitWithErrorMessage(ERROR_DOUBLE_MAP_LINE);
  }

  if (!playerDetailsExist) {
    exitWithErrorMessage(ERROR_PLAYER_LINE);
  }
};

import fs from "fs";
import { Map, Mountain, Treasure } from "../types/map";
import { Player } from "../types/user";

const writeFile = (map: Map, players: Player[]): string => {
  let text: string = "";

  text += `C - ${map.width} - ${map.height}\n`;

  map.mountains.forEach((mountain: Mountain) => {
    const { height, width } = mountain;

    text += `M - ${width} - ${height}\n`;
  });

  map.treasures.forEach((treasure: Treasure) => {
    const { height, width, numberOfTreasure } = treasure;

    if (numberOfTreasure) {
      text += `T - ${width} - ${height} - ${numberOfTreasure}\n`;
    }
  });

  players.forEach((player: Player) => {
    const { height, width, name, orientation, collectedTreasures } = player;

    text += `A - ${name} - ${width} - ${height} - ${orientation} - ${collectedTreasures}\n`;
  });

  return text;
};

export const generateResultFile = (
  filename: string,
  map: Map,
  players: Player[]
) => {
  fs.writeFile(`result-${filename}`, writeFile(map, players), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log(`File generated: result-${filename}`);
  });
};

export const exitWithErrorMessage = (message: string): void => {
  throw new Error(message);
};

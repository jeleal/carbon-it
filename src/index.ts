import fs from "fs";
import { movePlayer } from "./adventurer/movePlayers";
import { playersIsInTheMap } from "./adventurer/playersIsInTheMap";
import { playersStartAtSameBox } from "./adventurer/playersStartAtSameBox";
import { createTheMap } from "./map/createMap";
import { Box } from "./types/map";
import { generateResultFile } from "./utils/generateResultFile";
import { getData } from "./utils/getDataFromFile";
import { isValidData, isValidFilename } from "./utils/isValid";

const entryPoint = async () => {
  isValidFilename();
  const filename: string = process.argv[2];
  const data: string = fs.readFileSync(filename, "utf8");
  const lines: string[] = data.split("\n");

  isValidData(lines);
  const { players, map } = getData(lines);
  playersIsInTheMap(map, players);
  playersStartAtSameBox(players);
  const boxes: Box[][] = createTheMap(map, players);
  await movePlayer(map, players, boxes);
  generateResultFile(filename, map, players);
};

entryPoint();

import { Box, Map, Mountain, Treasure } from "../types/map";
import { Player } from "../types/user";
import { createLine } from "./createLine";

export const createTheMap = (map: Map, players: Player[]): Box[][] => {
  let createdMap: Box[][] = [];

  // Create the the map without the mountains, treasures and players
  for (let i = 0; i < map.height; i++) {
    createdMap.push(createLine(map));
  }

  map.mountains.forEach((mountain: Mountain) => {
    const { height, width } = mountain;

    if (createdMap[height] && createdMap[height][width]) {
      createdMap[height][width].type = "M";
    }
  });

  map.treasures.forEach((treasure: Treasure) => {
    const { height, width, numberOfTreasure } = treasure;

    if (createdMap[height] && createdMap[height][width] && numberOfTreasure) {
      createdMap[height][width].type = "T";
      createdMap[height][width].treasures = treasure.numberOfTreasure;
    }
  });

  players.forEach((player: Player) => {
    const { height, width } = player;

    if (createdMap[height] && createdMap[height][width]) {
      createdMap[height][width].type = "A";
    }
  });

  return createdMap;
};

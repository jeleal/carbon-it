import { Box } from "../types/map";

const printTabulation = (numberOfTabulations: number): string => {
  return "\t".repeat(numberOfTabulations);
};

export const printMap = (boxes: Box[][], numberOfTabulations: number) => {
  for (let heightIndex = 0; heightIndex < boxes.length; heightIndex++) {
    const boxByLine: Box[] = boxes[heightIndex];
    let printedLine: string = "";

    boxByLine.forEach((box: Box) => {
      const { treasures, type, player } = box;

      if (treasures > 0 && type === "T") {
        printedLine += `${type}(${treasures})${printTabulation(
          numberOfTabulations
        )}`;
      } else if (type === "A") {
        printedLine += `${type}(${player})${printTabulation(
          numberOfTabulations > 1
            ? numberOfTabulations - 1
            : numberOfTabulations
        )}`;
      } else {
        printedLine += `${type}${printTabulation(numberOfTabulations)}`;
      }
    });
    console.log(printedLine);
  }
};

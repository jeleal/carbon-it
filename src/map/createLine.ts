import { Box, Map } from "../types/map";

export const createLine = (map: Map): Box[] => {
  const row: Box[] = [];
  for (let currentWidth = 0; currentWidth < map.width; currentWidth++) {
    row.push({
      type: ".",
      treasures: 0,
      player: "",
    });
  }

  return row;
};

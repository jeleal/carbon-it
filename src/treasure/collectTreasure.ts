import { Box, Map, Treasure } from "../types/map";
import { Player } from "../types/user";

export const collectTreasure = (
  height: number,
  width: number,
  boxes: Box[][],
  player: Player,
  map: Map
) => {
  if (boxes[height][width].type === "T") {
    boxes[height][width].treasures -= 1;
    player.collectedTreasures += 1;
    if (boxes[height][width].treasures === 0) {
      boxes[height][width].type = ".";
    }
    map.treasures.forEach((treasure: Treasure) => {
      const { width: tWidth, height: tHeight } = treasure;
      if (tWidth === width && tHeight === height) {
        treasure.numberOfTreasure -= 1;
      }
    });
  }
};

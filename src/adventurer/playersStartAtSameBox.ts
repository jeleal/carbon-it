import { Player } from "../types/user";
import { exitWithErrorMessage } from "../utils/errorMessage";

export const playersStartAtSameBox = (players: Player[]) => {
  players.forEach((player: Player, index: number) => {
    const { width, height } = player;
    const playersCopy = players.filter((_, i) => i !== index);

    playersCopy.map((playerCopy: Player) => {
      const { width: widthCopy, height: heightCopy } = playerCopy;

      if (width === widthCopy && height === heightCopy) {
        exitWithErrorMessage(`Players start at the same box`);
      }
    });
  });
};

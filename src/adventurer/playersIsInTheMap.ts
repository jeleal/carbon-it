import { Map } from "../types/map";
import { Player } from "../types/user";
import { exitWithErrorMessage } from "../utils/errorMessage";

export const playersIsInTheMap = (map: Map, players: Player[]) => {
  players.forEach((player: Player) => {
    const { height, width, name, orientation, move } = player;

    if (height > map.height || width > map.width) {
      exitWithErrorMessage(
        `The player is not on the map: ` +
          `${name} - ${width} - ${height} - ${orientation} - ${move}`
      );
    }
  });
};

import { printMap } from "../map/printMap";
import { collectTreasure } from "../treasure/collectTreasure";
import { Box, Map } from "../types/map";
import { Player } from "../types/user";
import { wait } from "../utils/wait";

const adventurerCanMove = (
  height: number,
  width: number,
  boxes: Box[][]
): boolean => {
  if (boxes[height] && boxes[height][width]) {
    if (
      boxes[height][width].type !== "M" &&
      boxes[height][width].type !== "A"
    ) {
      return true;
    }
  }
  return false;
};

const moveForward = (player: Player, map: Map, boxes: Box[][]) => {
  const { width, height, orientation, name } = player;
  if (orientation === "N" && height > 0) {
    if (adventurerCanMove(height - 1, width, boxes)) {
      player.height -= 1;
      collectTreasure(height - 1, width, boxes, player, map);
      boxes[height][width].type =
        boxes[height][width].treasures > 0 ? "T" : ".";
      boxes[height][width].player = "";
      boxes[height - 1][width].type = "A";
      boxes[height - 1][width].player = name;
    }
  } else if (orientation === "S" && height < map.height) {
    if (adventurerCanMove(height + 1, width, boxes)) {
      player.height += 1;
      collectTreasure(height + 1, width, boxes, player, map);
      boxes[height][width].type =
        boxes[height][width].treasures > 0 ? "T" : ".";
      boxes[height][width].player = "";
      boxes[height + 1][width].type = "A";
      boxes[height + 1][width].player = name;
    }
  } else if (orientation === "E" && width > 0) {
    if (adventurerCanMove(height, width + 1, boxes)) {
      player.width += 1;
      collectTreasure(height, width + 1, boxes, player, map);
      boxes[height][width].type =
        boxes[height][width].treasures > 0 ? "T" : ".";
      boxes[height][width].player = "";
      boxes[height][width + 1].type = "A";
      boxes[height][width + 1].player = name;
    }
  } else if (orientation === "O" && width < map.width) {
    if (adventurerCanMove(height, width - 1, boxes)) {
      player.width -= 1;
      collectTreasure(height, width - 1, boxes, player, map);
      boxes[height][width].type =
        boxes[height][width].treasures > 0 ? "T" : ".";
      boxes[height][width].player = "";
      boxes[height][width - 1].type = "A";
      boxes[height][width - 1].player = name;
    }
  }
};

const moveRight = (player: Player) => {
  const { orientation } = player;

  if (orientation === "N") {
    player.orientation = "E";
  } else if (orientation === "S") {
    player.orientation = "O";
  } else if (orientation === "E") {
    player.orientation = "S";
  } else if (orientation === "O") {
    player.orientation = "N";
  }
};

const moveLeft = (player: Player) => {
  const { orientation } = player;

  if (orientation === "N") {
    player.orientation = "O";
  } else if (orientation === "S") {
    player.orientation = "E";
  } else if (orientation === "E") {
    player.orientation = "N";
  } else if (orientation === "O") {
    player.orientation = "S";
  }
};

export const movePlayer = async (
  map: Map,
  players: Player[],
  boxes: Box[][]
) => {
  for (let i = 0; i < map.loopNumber; i++) {
    await wait(1250);
    console.clear();
    players.forEach((player: Player) => {
      if (player.move[i] === "A") {
        moveForward(player, map, boxes);
      } else if (player.move[i] === "D") {
        moveRight(player);
      } else if (player.move[i] === "G") {
        moveLeft(player);
      }
    });
    printMap(boxes, map.numberOfTabulations);
    console.log(`\nRound: ${i}`);
  }
};

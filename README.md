# La carte aux trésors

## Project setup

```bash
npm install
```

## Compiles and hot-reloads for development

```bash
# You must specify the file name for the map
npm run dev -- "filename.txt"
```

## Compiles and minifies for production

```bash
# You must specify the file name for the map
npm run start -- "filename.txt"
```

## Test

```bash
npm run test
```

export const formatedErrorMessage = (
  type: string,
  index: number,
  line: string
): string => `Invalid ${type} for the line ${index + 1}: ${line}`;

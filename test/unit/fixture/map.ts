export const MAP = {
  height: 4,
  loopNumber: 9,
  mountains: [
    {
      height: 0,
      width: 1,
    },
    {
      height: 1,
      width: 2,
    },
  ],
  numberOfTabulations: 1,
  treasures: [
    {
      height: 3,
      numberOfTreasure: 2,
      width: 0,
    },
    {
      height: 3,
      numberOfTreasure: 3,
      width: 1,
    },
  ],
  width: 3,
};

export const PLAYERS = [
  {
    collectedTreasures: 0,
    height: 1,
    move: "AADADAGGA",
    name: "Lara",
    orientation: "S",
    width: 1,
  },
  {
    collectedTreasures: 0,
    height: 2,
    move: "AGGADADAA",
    name: "Mara",
    orientation: "S",
    width: 1,
  },
];

export const PLAYER_OUT_OF_MAP = {
  collectedTreasures: 0,
  height: 10,
  move: "AADADAGGA",
  name: "Lara",
  orientation: "S",
  width: 10,
};

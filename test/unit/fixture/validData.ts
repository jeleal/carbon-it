export const MAP_DATA = [
  "C​ - 3 - 4",
  "M​ - 1 - 0",
  "M​ - 2 - 1",
  "T​ - 0 - 3 - 2",
  "T​ - 1 - 3 - 3",
  "A​ - Lara - 1 - 1 - S - AADADAGGA",
];

export const MAP_LINE = "C​ - 3 - 4";

export const MOUNTAIN_LINE = "M​ - 1 - 0";

export const TREASURE_LINE = "T​ - 0 - 3 - 2";

export const PLAYER_LINE = "A​ - Lara - 1 - 1 - S - AADADAGGA";

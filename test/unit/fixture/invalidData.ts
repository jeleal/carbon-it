export const MAP_DATA = [
  "C​ - 3 - 4",
  "M​ - 1 - 0",
  "M​ - 2 - 1",
  "T​ - 0 - 3 - 2",
  "T​ - 1 - 3 - 3",
  "A​ - Lara - 1 - 1 - S - AADADAGGA",
];

export const INVALID_LENGTH_LINE = "X - 3";
export const INVALID_LINE = "X - 3 - 4";

// Map
export const INVALID_C_HEIGHT_NUMBER = "C - X - 3";
export const INVALID_C_WIDTH_NUMBER = "C - 0 - X";
export const INVALID_C_WIDTH_AND_HEIGHT = "C - 0 - X";
export const INVALID_COORDINATES = "C - 0 - 0";

// Mountain
export const INVALID_M_HEIGHT_NUMBER = "M - X - 3";
export const INVALID_M_WIDTH_NUMBER = "M - 0 - X";

// Player
export const INVALID_A_HEIGHT_NUMBER = "A - LARA - X - 1 - S - AADASAGGA";
export const INVALID_A_WIDTH_NUMBER = "A - LARA - 1 - X - S - AADASAGGA";
export const INVALID_ORIENTATION = "A - LARA - 1 - 1 - X - AADASAGGA";
export const INVALID_MOVE = "A - LARA - 1 - 2 - S - AADAXAGGA";

// Treasure
export const INVALID_T_HEIGHT_NUMBER = "T​ - X - 3 - 2";
export const INVALID_T_WIDTH_NUMBER = "T​ - 0 - X - 2";
export const INVALID_TREASURE_NUMBER = "T​ - 0 - 3 - X";

export const BOXES = [
  [
    {
      player: "",
      treasures: 0,
      type: ".",
    },
    {
      player: "",
      treasures: 0,
      type: "M",
    },
    {
      player: "",
      treasures: 0,
      type: ".",
    },
  ],
  [
    {
      player: "",
      treasures: 0,
      type: ".",
    },
    {
      player: "",
      treasures: 0,
      type: "A",
    },
    {
      player: "",
      treasures: 0,
      type: "M",
    },
  ],
  [
    {
      player: "",
      treasures: 0,
      type: ".",
    },
    {
      player: "",
      treasures: 0,
      type: ".",
    },
    {
      player: "",
      treasures: 0,
      type: ".",
    },
  ],
  [
    {
      player: "",
      treasures: 2,
      type: "T",
    },
    {
      player: "",
      treasures: 3,
      type: "T",
    },
    {
      player: "",
      treasures: 0,
      type: ".",
    },
  ],
];

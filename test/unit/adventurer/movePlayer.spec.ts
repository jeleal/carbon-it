import { movePlayer } from "../../../src/adventurer/movePlayers";
import { PLAYERS } from "../fixture/players";
import { MAP } from "../fixture/map";
import { BOXES } from "../fixture/boxes";

describe("movePlayer()", () => {
  it(
    "should the final result for map and players",
    async () => {
      // This line is neccesary to hide all console.log
      jest.spyOn(global.console, "log").mockImplementation();
      await movePlayer(MAP, PLAYERS, BOXES);
      expect({ map: MAP, players: PLAYERS }).toMatchSnapshot();
    },
    MAP.loopNumber * 1300
  );
});

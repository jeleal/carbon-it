import { playersStartAtSameBox } from "../../../src/adventurer/playersStartAtSameBox";
import { PLAYERS } from "../fixture/players";

describe("playersStartAtSameBox()", () => {
  it("should throw error for players start at the same box", () => {
    expect(() => {
      playersStartAtSameBox([PLAYERS[0], PLAYERS[0]]);
    }).toThrow(`Players start at the same box`);
  });

  it("should run without error", () => {
    expect(() => {
      playersStartAtSameBox(PLAYERS);
    }).not.toThrow();
  });
});

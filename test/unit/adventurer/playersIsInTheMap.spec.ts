import { playersIsInTheMap } from "../../../src/adventurer/playersIsInTheMap";
import { MAP } from "../fixture/map";
import { PLAYERS, PLAYER_OUT_OF_MAP } from "../fixture/players";

describe("playersIsInTheMap()", () => {
  it("should throw error for player out of the map", () => {
    const { height, width, name, orientation, move } = PLAYER_OUT_OF_MAP;

    expect(() => {
      playersIsInTheMap(MAP, [PLAYER_OUT_OF_MAP]);
    }).toThrow(
      `The player is not on the map: ` +
        `${name} - ${width} - ${height} - ${orientation} - ${move}`
    );
  });

  it("should run without error", () => {
    expect(() => {
      playersIsInTheMap(MAP, PLAYERS);
    }).not.toThrow();
  });
});

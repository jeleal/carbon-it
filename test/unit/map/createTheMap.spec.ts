import { createTheMap } from "../../../src/map/createMap";
import { PLAYERS } from "../fixture/players";
import { MAP } from "../fixture/map";

describe("createTheMap()", () => {
  it("should return the boxes for a map", () => {
    expect(createTheMap(MAP, PLAYERS)).toMatchSnapshot();
  });
});

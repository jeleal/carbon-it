import { isValidTreasure } from "../../../src/utils/isValid";
import { formatedErrorMessage } from "../fixture/formatedErrorMessage";
import {
  INVALID_LINE,
  INVALID_T_HEIGHT_NUMBER,
  INVALID_T_WIDTH_NUMBER,
  INVALID_TREASURE_NUMBER,
} from "../fixture/invalidData";
import { TREASURE_LINE } from "../fixture/validData";

describe("isValidTreasure()", () => {
  it("should throw error for invalid type T", () => {
    expect(() => {
      isValidTreasure(INVALID_LINE.split(" - "), 0);
    }).toThrow(formatedErrorMessage("treasure", 0, INVALID_LINE));
  });

  it("should throw invalid WIDTH error for type T", () => {
    expect(() => {
      isValidTreasure(INVALID_T_WIDTH_NUMBER.split(" - "), 0);
    }).toThrow(formatedErrorMessage("treasure", 0, INVALID_T_WIDTH_NUMBER));
  });

  it("should throw invalid HEIGHT error for type T", () => {
    expect(() => {
      isValidTreasure(INVALID_T_HEIGHT_NUMBER.split(" - "), 0);
    }).toThrow(formatedErrorMessage("treasure", 0, INVALID_T_HEIGHT_NUMBER));
  });

  it("should throw invalid TREASURE error for type T", () => {
    expect(() => {
      isValidTreasure(INVALID_TREASURE_NUMBER.split(" - "), 0);
    }).toThrow(formatedErrorMessage("treasure", 0, INVALID_TREASURE_NUMBER));
  });

  it("should run without error", () => {
    expect(() => {
      isValidTreasure(TREASURE_LINE.split(" - "), 0);
    }).not.toThrow();
  });
});

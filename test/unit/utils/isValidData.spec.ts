import {
  ERROR_DOUBLE_MAP_LINE,
  ERROR_MAP_LINE,
} from "../../../src/constant/mapError";
import { ERROR_PLAYER_LINE } from "../../../src/constant/playerError";
import { isValidData } from "../../../src/utils/isValid";
import { formatedErrorMessage } from "../fixture/formatedErrorMessage";
import { INVALID_LENGTH_LINE } from "../fixture/invalidData";
import {
  MAP_LINE,
  MOUNTAIN_LINE,
  TREASURE_LINE,
  PLAYER_LINE,
  MAP_DATA,
} from "../fixture/validData";

describe("isValidData()", () => {
  it("should throw error for empty data", () => {
    expect(() => {
      isValidData([""]);
    }).toThrow(ERROR_MAP_LINE);
  });

  it("should throw error for data without line C", () => {
    expect(() => {
      isValidData([MOUNTAIN_LINE, TREASURE_LINE, PLAYER_LINE]);
    }).toThrow(ERROR_MAP_LINE);
  });

  it("should throw error for data without line A", () => {
    expect(() => {
      isValidData([MAP_LINE, MOUNTAIN_LINE, TREASURE_LINE]);
    }).toThrow(ERROR_PLAYER_LINE);
  });

  it("should throw error for data with two lines C", () => {
    expect(() => {
      isValidData([MAP_LINE, MAP_LINE]);
    }).toThrow(ERROR_DOUBLE_MAP_LINE);
  });

  it("should throw error for a invalid lenght line", () => {
    expect(() => {
      isValidData([INVALID_LENGTH_LINE]);
    }).toThrow(formatedErrorMessage("parameters", 0, INVALID_LENGTH_LINE));
  });

  it("should run without error", () => {
    expect(() => {
      isValidData(MAP_DATA);
    }).not.toThrow();
  });
});

import { isValidAdventurer } from "../../../src/utils/isValid";
import { formatedErrorMessage } from "../fixture/formatedErrorMessage";
import {
  INVALID_LINE,
  INVALID_A_HEIGHT_NUMBER,
  INVALID_A_WIDTH_NUMBER,
  INVALID_ORIENTATION,
  INVALID_MOVE,
} from "../fixture/invalidData";
import { PLAYER_LINE } from "../fixture/validData";

describe("isValidAdventurer()", () => {
  it("should throw error for invalid type A", () => {
    expect(() => {
      isValidAdventurer(INVALID_LINE.split(" - "), 0);
    }).toThrow(formatedErrorMessage("adventurer", 0, INVALID_LINE));
  });

  it("should throw invalid WIDTH error for type A", () => {
    expect(() => {
      isValidAdventurer(INVALID_A_WIDTH_NUMBER.split(" - "), 0);
    }).toThrow(formatedErrorMessage("adventurer", 0, INVALID_A_WIDTH_NUMBER));
  });

  it("should throw invalid HEIGHT error for type A", () => {
    expect(() => {
      isValidAdventurer(INVALID_A_HEIGHT_NUMBER.split(" - "), 0);
    }).toThrow(formatedErrorMessage("adventurer", 0, INVALID_A_HEIGHT_NUMBER));
  });

  it("should throw invalid ORIENTATION error for type A", () => {
    expect(() => {
      isValidAdventurer(INVALID_ORIENTATION.split(" - "), 0);
    }).toThrow(formatedErrorMessage("adventurer", 0, INVALID_ORIENTATION));
  });

  it("should run without error", () => {
    expect(() => {
      isValidAdventurer(PLAYER_LINE.split(" - "), 0);
    }).not.toThrow();
  });

  it("should throw invalid MOVE error for type T", () => {
    expect(() => {
      isValidAdventurer(INVALID_MOVE.split(" - "), 0);
    }).toThrow(formatedErrorMessage("move", 0, INVALID_MOVE));
  });
});

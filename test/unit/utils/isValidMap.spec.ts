import { isValidMap } from "../../../src/utils/isValid";
import { formatedErrorMessage } from "../fixture/formatedErrorMessage";
import {
  INVALID_LINE,
  INVALID_C_HEIGHT_NUMBER,
  INVALID_C_WIDTH_NUMBER,
  INVALID_COORDINATES,
} from "../fixture/invalidData";
import { MAP_LINE } from "../fixture/validData";

describe("isValidMap()", () => {
  it("should throw error for invalid type C", () => {
    expect(() => {
      isValidMap(INVALID_LINE.split(" - "), 0);
    }).toThrow(formatedErrorMessage("parameters", 0, INVALID_LINE));
  });

  it("should throw invalid WIDTH error for type C", () => {
    expect(() => {
      isValidMap(INVALID_C_WIDTH_NUMBER.split(" - "), 0);
    }).toThrow(formatedErrorMessage("map", 0, INVALID_C_WIDTH_NUMBER));
  });

  it("should throw invalid HEIGHT error for type C", () => {
    expect(() => {
      isValidMap(INVALID_C_HEIGHT_NUMBER.split(" - "), 0);
    }).toThrow(formatedErrorMessage("map", 0, INVALID_C_HEIGHT_NUMBER));
  });

  it("should throw invalid error for coordinates 0 - 0", () => {
    expect(() => {
      isValidMap(INVALID_COORDINATES.split(" - "), 0);
    }).toThrow(formatedErrorMessage("map", 0, INVALID_COORDINATES));
  });

  it("should run without error", () => {
    expect(() => {
      isValidMap(MAP_LINE.split(" - "), 0);
    }).not.toThrow();
  });
});

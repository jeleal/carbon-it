import { getData } from "../../../src/utils/getDataFromFile";
import { MAP_DATA } from "../fixture/validData";

describe("getData()", () => {
  it("should return players and map from the string array", () => {
    expect(getData(MAP_DATA)).toMatchSnapshot();
  });
});

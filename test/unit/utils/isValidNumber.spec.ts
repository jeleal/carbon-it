import { isValidNumber } from "../../../src/utils/isValid";

describe("isValidNumber()", () => {
  it("should be return false", () => {
    expect(isValidNumber("")).toBe(false);
  });

  it("should be return true", () => {
    expect(isValidNumber("150")).toBe(true);
  });
});

module.exports = {
  preset: "ts-jest",
  coverageDirectory: "./test/coverage",
  testMatch: ["**/test/**/**/*.spec.ts"],
};
